# Hypertube

# Getting Started

* Create or select an issue
* Create a merge request of the issue
* Assigned it to yourself
* `git pull`
* `git checkout [YOUR-BRANCH]`

## How to push for merge

* `git add [YOUR-MODIFIED-FILES]`
* `git commit -m "[YOUR-MESSAGE]"`
* `git checkout master`
* `git pull`
* `git checkout [YOUR-BRANCH]`
* `git rebase master -i` (don't forget to write `master` !)
* Replace the words `pick` by `s` or `squash`, except for the first line (don't touch it)
* Save it
* `git push -f`
* Put a label 'review requested' on gitlab on your merge request.
* Assign a person who need to review it.
* And work on another issue waiting that someone reviewed it.

# How to fix conflict

* Example of conflict:

```  
First, rewinding head to replay your work on top of it...
Applying: removed package
Using index info to reconstruct a base tree...
M	src/quiz/components/Answer.js
M	src/quiz/components/Question.js
M	src/quiz/screens/QuizScreen.js
Falling back to patching base and 3-way merge...
Auto-merging src/quiz/screens/QuizScreen.js
CONFLICT (content): Merge conflict in src/quiz/screens/QuizScreen.js
Auto-merging src/quiz/components/Question.js
Auto-merging src/quiz/components/Answer.js
Removing android/app/src/main/java/com/quidol/detecthardware/DetectHardwarePackage.java
Removing android/app/src/main/java/com/quidol/detecthardware/DetectHardwareModule.java
error: Failed to merge in the changes.
Patch failed at 0001 removed package
The copy of the patch that failed is found in: .git/rebase-apply/patch

Resolve all conflicts manually, mark them as resolved with
"git add/rm <conflicted_files>", then run "git rebase --continue".
You can instead skip this commit: run "git rebase --skip".
To abort and get back to the state before "git rebase", run "git rebase --abort".
```  

* Resolve all conflicts manually 
* Then `git add [YOUR-MODIFIED-FILES]`
* `git rebase --continue`
* Fixer les conflits ...
* `git add [YOUR-MODIFIED-FILES]`
* `git rebase --continue`
* If there is another conflict, apply the same process
* When it shows : `Applying: [YOUR-COMMIT-MESSAGE]`, test your features again to check that everything it's ok
* Then `git push -f`

# Undo a rebase
* If you want to undo a rebase, just `git rebase --abort`